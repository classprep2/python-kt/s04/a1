# 1 Create an abstract class called Animal
	# Abstraction
		# Hide unnecessary details from the user

from abc import ABC, abstractclassmethod

#Parent
class Animal(ABC):
	@abstractclassmethod

	
	def eat(self, food):
		#pass statement is used as placeholder for future code
		pass
		
	def make_sound(self):
		pass

 
# 2 Create two classes that implements the Animal class called Cat and Dog

#Child 1
class Dog(Animal):
	# The _init_ function is called every time an object is created from a class
	# Lets the class initialize the object's attributes and serves no other purpose
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed;
		self._age = age;

	# setters
	def set_name(self, name):
		self._name = name

	def set_age(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age


	# getters
	def get_name(self):
		print(f"Name: {self._name}")

	def get_breed(breed):
		print(f"Breed: {self._breed}")

	def get_age(self):
		print(f"Age: {self._age}")

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! Woof! Arg!")

	def call(self):
		print(f"Here {self._name}!")


#Child 2
class Cat(Animal):
	# The _init_ function is called every time an object is created from a class
	# Lets the class initialize the object's attributes and serves no other purpose
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed;
		self._age = age;

	# setters
	def set_name(self, name):
		self._name = name

	def set_age(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age


	# getters
	def get_name(self):
		print(f"Name: {self._name}")

	def get_breed(breed):
		print(f"Breed: {self._breed}")

	def get_age(self):
		print(f"Age: {self._age}")

	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self._name}, come on!")


print("---Dog---")
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

#Try a setter
print("---Try a setter and getter---")
dog1.set_name("Maze")
dog1.call()

print("==============")

print("---Cat---")
cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

#Try a setter
print("---Try a setter and getter---")
cat1.set_name("Majin")
cat1.call()
	